FROM node:alpine as build-stage

COPY . /app/

WORKDIR /app

RUN yarn global add @angular/cli && \
	yarn && \
	ng build --prod

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:alpine

WORKDIR /usr/share/nginx/html

COPY --from=build-stage /app/dist/app-one-frontend/ .
COPY nginx.conf /etc/nginx/nginx.conf
