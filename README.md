# AppOneFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.4.

# Initial setup
- add env. vars to Gitlab CICD required to run pipeline (listed bellow)
- add secrets to K8s cluster that are required in pod
- any commit will triger CICD and push to Prod env.


# Running locally:

## Docker
- 2 options:
	- self build Docker image via Docker file:
		- `docker build -t app-one-frontend:v1 .`
	- reuse image already build by cicd pipeline in Gitlab registry:
		- `docker login -u <username> -p <pwd_or_token> registry.gitlab.com`
		- `docker pull <image:tag>`
		- `docker run -it --rm <image:tag> /bin/sh`


## Minikube
- setup minikube
- via kubectl create all necessary secrets (listed bellow)
- `vi minikube_testing/minikube_deployment.yaml` and adjust the `image version` to get correct image
- `minikube_deployment.yaml` will setup simple Postgres DB, you'll need to run *database migration* first
- run the `minikube_deployment.yaml` + `service/ingress yamls` if desired


### Notes:
- if you want to use your private gitlab registry docker image login with docker:
	- `kubectl create secret docker-registry gitlab-registry-auth --docker-server=registry.gitlab.com --docker-username=testuser --docker-password=gimmepwd --docker-email=gimme@email.com`
	- minikube deployment automatically looks for `gitlab-registry-auth`


## gitlab CICD env. vars.

`STAGING_CLUSTER_NAME=gke-default-pool-0b2284e5-tv00
STAGING_URL=35.198.160.000
KUBE_CRED_NAME=app-one-admin
STAGING_KUBE_PASSWORD=gimmepwd`

## K8s secrets
`kubectl create secret docker-registry gitlab-registry-auth --docker-server=registry.gitlab.com --docker-username=testuser --docker-password=gimmepwd --docker-email=gimme@email.com`


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Dependencies:
npm install --save @angular/http
npm install --save @ng-bootstrap/ng-bootstrap

## Documentation:
https://medium.com/craft-academy/connecting-an-api-to-an-angular-4-front-end-application-e0fc9ea33202