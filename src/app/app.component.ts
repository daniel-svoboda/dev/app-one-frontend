import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app-one-frontend';
  private apiUrl = 'https://awesometest.ga/api/v1/app-one-backend/users/';
  data: any = {};

  constructor(private http: Http) {
  	console.log('Here is the list of users:');
  	this.getContacts();
  	this.getData();
  }

getData() {
	return this.http.get(this.apiUrl).pipe(map((res: Response) => res.json()))
}

getContacts() {
	this.getData().subscribe(data => {
		console.log(data);
		this.data = data
		})
	}
}

